<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;

class EditController extends Controller
{
    public function index(User $user)
    {
        $education_type_list = User::EDUCATION_TYPE_LIST;
        return view('pages.edit-form')
            ->with(compact(
                'user',
                'education_type_list'
            ));
    }

    public function edit(UserFormRequest $request, User $user): RedirectResponse
    {
        $attributes = $request->validated();
        $user->update($attributes);
        return redirect()
            ->back()
            ->with('success', 'Данные пользователя отредактированы.');
    }

    public function remove(User $user): RedirectResponse
    {
        $user->scoreResult()->delete();
        $user->delete();
        return redirect()
            ->back()
            ->with('success', 'Пользователь успешно удален.');
    }
}
