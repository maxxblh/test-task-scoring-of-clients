<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;

class RegisterController extends Controller
{
    public function index()
    {
        $education_type_list = User::EDUCATION_TYPE_LIST;
        return view('pages.register-form')
            ->with(compact('education_type_list'));
    }

    public function register(UserFormRequest $request): RedirectResponse
    {
        $attributes = $request->validated();
        User::create($attributes);

        return redirect()
            ->back()
            ->with('success', 'Регистрация прошла успешно.');
    }
}
