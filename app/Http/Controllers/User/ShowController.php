<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ShowController extends Controller
{
    public function __invoke(): Factory|View|Application
    {
        $users = User::with('scoreResult')
            ->latest()
            ->paginate(10);

        return view('pages.user-table')
            ->with(compact('users'));
    }
}
