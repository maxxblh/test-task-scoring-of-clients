<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;
use Illuminate\Validation\Validator;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'firstname' => [
                'required',
                'string',
                'max:64',
            ],
            'lastname'  => [
                'required',
                'string',
                'max:64',
            ],
            'phone'     => [
                'required',
                'string',
                'regex:/^(89)[\d]{9}$/',
                $this->getRuleUnique('phone'),
            ],
            'email'     => [
                'required',
                'email:rfc,dns',
                $this->getRuleUnique('email'),
            ],
            'education' => [
                'required',
                'int',
            ],
            'agree'     => [
                'required',
                'int',
                'between:0,1',
                ],
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param Validator $validator
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            $education = (int)$this->get('education');

            if (!in_array($education, array_keys(User::EDUCATION_TYPE_LIST))) {
                $validator->errors()->add('education', 'Выбран некорректный тип образования.');
            }
        });
    }

    public function messages(): array
    {
        return [
            'required'    => 'Поле :attribute необходимо заполнить',
            'string'      => 'Неверный тип',
            'int'         => 'Неверный тип',
            'between'     => 'Некорректное значение',
            'bool'        => 'Неверный тип',
            'max'         => 'Превышено максимальное количество символов',
            'phone.regex' => 'Неверный формат номера телефона',
            'email'       => 'Неверный email',
            'unique'      => 'Данный :attribute уже используется',
        ];
    }

    public function attributes(): array
    {
        return [
            'firstname' => 'имя',
            'lastname'  => 'фамилия',
            'phone'     => 'телефон',
            'education' => 'образование',
            'agree'     => 'пункт согласен',
        ];
    }

    private function getRuleUnique(string $column): Unique
    {
        $rule = Rule::unique('users', $column);
        if ($this->isUpdateOfUser()) {
            $rule->ignore($this->user->id);
        }
        return $rule;
    }

    private function isUpdateOfUser(): bool
    {
        return $this->getMethod() === 'PUT';
    }
}
