<?php

namespace App\Jobs;

use App\Models\User;
use App\Services\ScoreResultService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CalculateScoreJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private User $user;

    /**
     * Create a new job instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param ScoreResultService $scoreService
     * @return void
     */
    public function handle(ScoreResultService $scoreService)
    {
        Log::info(self::class . ': start calculate score for user_id = ' . $this->user->id);
        $scoreService->calculate($this->user);
        Log::info(self::class . ': scores calculating is finished.');
    }
}
