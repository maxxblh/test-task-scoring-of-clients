<?php


namespace App\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class KodyClientService
{
    private string $token;
    private string $uri;
    private Client $client;

    public function __construct(Client $client, string $token, string $uri)
    {
        $this->client = $client;
        $this->token = $token;
        $this->uri = $uri;
    }

    /**
     * @throws GuzzleException
     */
    public function getOperatorName(string $phone): string
    {
        $operator_name = '';
        try {
            $response = $this->client->get($this->uri, $this->getParams($phone));
            $content = json_decode($response->getBody()->getContents());
            if ($content->success && $content->numbers) {
                $number_info = $content->numbers[0];
                if ($number_info->number_success) {
                    $operator_name = empty($number_info->bdpn_operator)
                        ? $number_info->operator
                        : $number_info->bdpn_operator;
                }
            }
        } catch (BadResponseException $e) {
            Log::error('Произошла ошибка во время выполнения запроса к API Kody.su', [
                'status_code' => $e->getCode(),
                'message'     => $e->getMessage(),
            ]);
        } catch (ConnectException $e) {
            Log::error('Не удается установить соединение с Kody.su', [
                'message' => $e->getMessage(),
            ]);
        }

        return $operator_name;
    }

    private function getParams(string $phone): array
    {
        return [
            'query' => [
                'q'   => $phone,
                'key' => $this->token,
            ]
        ];
    }
}
