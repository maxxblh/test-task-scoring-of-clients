<?php

namespace App\Services\ScoreRules;

abstract class AbstractScoreRuleService
{
    abstract protected function getRules(): array;

    abstract protected function getDefaultScore(): int;

    public function getScore($value)
    {
        $rules = $this->getRules();
        if (array_key_exists($value, $rules)) {
            return $rules[$value];
        }

        return $this->getDefaultScore();
    }
}
