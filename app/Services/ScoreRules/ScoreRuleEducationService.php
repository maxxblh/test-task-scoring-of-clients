<?php

namespace App\Services\ScoreRules;

use App\Models\User;

class ScoreRuleEducationService extends AbstractScoreRuleService
{
    private array $rules = [
        User::EDUCATION_TYPE_HIGHER    => 15,
        User::EDUCATION_TYPE_SPECIAL   => 10,
        User::EDUCATION_TYPE_SECONDARY => 5,
    ];

    private int $default_score = 0;

    protected function getRules(): array
    {
        return $this->rules;
    }

    protected function getDefaultScore(): int
    {
        return $this->default_score;
    }
}
