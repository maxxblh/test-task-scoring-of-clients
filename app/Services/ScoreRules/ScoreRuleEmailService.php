<?php

namespace App\Services\ScoreRules;

class ScoreRuleEmailService extends AbstractScoreRuleService
{
    private array $rules = [
        'gmail.com' => 10,
        'yandex.ru' => 8,
        'mail.ru'   => 6,
    ];

    private int $default_score = 3;

    protected function getRules(): array
    {
        return $this->rules;
    }

    protected function getDefaultScore(): int
    {
        return $this->default_score;
    }
}
