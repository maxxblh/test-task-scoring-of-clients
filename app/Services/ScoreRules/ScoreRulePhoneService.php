<?php

namespace App\Services\ScoreRules;

class ScoreRulePhoneService extends AbstractScoreRuleService
{
    private array $rules = [
        'Мегафон' => 10,
        'Билайн'  => 5,
        'МТС'     => 3,
    ];

    private int $default_score = 1;

    protected function getRules(): array
    {
        return $this->rules;
    }

    protected function getDefaultScore(): int
    {
        return $this->default_score;
    }
}
