<?php

namespace App\Services\ScoreRules;

class ScoreRuleAgreeService extends AbstractScoreRuleService
{
    private array $rules = [
        1 => 4,
        0 => 0,
    ];

    private int $default_score = 0;

    protected function getRules(): array
    {
        return $this->rules;
    }

    protected function getDefaultScore(): int
    {
        return $this->default_score;
    }
}
