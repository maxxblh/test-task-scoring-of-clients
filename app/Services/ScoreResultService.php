<?php

namespace App\Services;

use App\Models\ScoreResult;
use App\Models\User;
use App\Services\ScoreRules\ScoreRuleAgreeService;
use App\Services\ScoreRules\ScoreRuleEducationService;
use App\Services\ScoreRules\ScoreRuleEmailService;
use App\Services\ScoreRules\ScoreRulePhoneService;
use Illuminate\Database\Eloquent\Collection;

class ScoreResultService
{
    public function calculate(User $user)
    {
        ScoreResult::updateOrCreate(
            [
                'user_id' => $user->id,
            ],
            [
                'score_phone'     => $this->getScoreForPhone($user->phone),
                'score_email'     => $this->getScoreForEmail($user->email),
                'score_education' => $this->getScoreForEducation($user->education),
                'score_agree'     => $this->getScoreForAgreeMark($user->agree),
            ]
        );
    }

    public function getScoreDetails(int|null $user_id = null): Collection
    {
        $builder = ScoreResult::select([
            'user_id',
            'score_phone',
            'score_email',
            'score_education',
            'score_agree',
        ]);
        if (!is_null($user_id)) {
            $builder->whereUserId($user_id);
        }

        return $builder->get();
    }

    private function getScoreForPhone(string $phone): int
    {
        $operator_name = app(KodyClientService::class)->getOperatorName($phone);
        return app(ScoreRulePhoneService::class)->getScore($operator_name);
    }

    private function getScoreForEmail(string $email): int
    {
        return app(ScoreRuleEmailService::class)->getScore($this->getEmailDomain($email));
    }

    private function getScoreForEducation(int $education): int
    {
        return app(ScoreRuleEducationService::class)->getScore($education);
    }

    private function getScoreForAgreeMark(int $agree): int
    {
        return app(ScoreRuleAgreeService::class)->getScore($agree);
    }

    private function getEmailDomain(string $email): string
    {
        $email_parts = explode('@', $email);
        return array_pop($email_parts);
    }
}
