<?php

namespace App\Providers;

use App\Models\User;
use App\Observers\UserObserver;
use App\Services\KodyClientService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(KodyClientService::class, function () {
            return new KodyClientService(
                new Client(),
                config('services.kody.token'),
                config('services.kody.uri'),
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
    }
}
