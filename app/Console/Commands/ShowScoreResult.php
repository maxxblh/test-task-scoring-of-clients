<?php

namespace App\Console\Commands;

use App\Services\ScoreResultService;
use Illuminate\Console\Command;

class ShowScoreResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'score:show {user_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show score results of users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ScoreResultService $scoreService
     * @return void
     */
    public function handle(ScoreResultService $scoreService)
    {

        $data = $scoreService->getScoreDetails($this->argument('user_id'));
        if ($data->isNotEmpty()) {
            $this->table($this->getTableHeaders(), $data);
        } else {
            $this->info('Данные отсутствуют');
        }
    }

    private function getTableHeaders(): array
    {
        return [
            'ID пользователя',
            'Баллы за телефон',
            'Баллы за email',
            'Баллы за образование',
            'Баллы за пункт согласен',
            'Всего баллов'
        ];
    }
}
