<?php

namespace App\Observers;

use App\Jobs\CalculateScoreJob;
use App\Models\User;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param User $user
     * @return void
     */
    public function created(User $user)
    {
        CalculateScoreJob::dispatch($user);
    }

    /**
     * Handle the User "updated" event.
     *
     * @param User $user
     * @return void
     */
    public function updated(User $user)
    {
        CalculateScoreJob::dispatch($user);
    }
}
