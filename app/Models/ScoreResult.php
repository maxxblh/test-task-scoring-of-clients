<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ScoreResult
 *
 * @property int $id
 * @property int $user_id
 * @property int $score_phone
 * @property int $score_email
 * @property int $score_education
 * @property int $score_agree
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read int $score_total
 * @method static \Database\Factories\ScoreResultFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult query()
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult whereScoreAgree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult whereScoreEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult whereScoreEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult whereScorePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ScoreResult whereUserId($value)
 * @mixin \Eloquent
 */
class ScoreResult extends Model
{
    use HasFactory;

    protected $table = 'score_results';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'score_phone',
        'score_email',
        'score_education',
        'score_agree',
    ];

    protected $appends = ['score_total'];

    public function getScoreTotalAttribute(): int
    {
        $score_items = collect([
            $this->score_phone,
            $this->score_email,
            $this->score_education,
            $this->score_agree,
        ]);
        return $score_items->sum();
    }
}
