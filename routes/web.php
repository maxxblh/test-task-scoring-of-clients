<?php

use App\Http\Controllers\User\RegisterController;
use App\Http\Controllers\User\EditController;
use App\Http\Controllers\User\ShowController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/registration', [RegisterController::class, 'index'])
    ->name('user.register.index');

Route::post('/register', [RegisterController::class, 'register'])
    ->name('user.register');

Route::get('/users', ShowController::class)
    ->name('user.index');

Route::get('/users/{user}', [EditController::class, 'index'])
    ->name('user.edit.index');

Route::put('/users/edit/{user}', [EditController::class, 'edit'])
    ->name('user.edit');

Route::get('/users/remove/{user}', [EditController::class, 'remove'])
    ->name('user.remove');
