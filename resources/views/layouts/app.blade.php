<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Test task</title>
</head>

<body>
<div class="container">
    <header>
        @include('includes.header')
    </header>
    @include('includes.flash-message')
    @yield('content')
</div>
</body>
@stack('scripts')
</html>
