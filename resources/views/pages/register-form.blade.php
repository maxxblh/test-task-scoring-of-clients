@extends('layouts.app')

@section('content')
    <div class="max-w-2xl mx-auto bg-white p-16">
        <h1 class="font-bold text-xl text-indigo-600 flex justify-center items-center mb-6">Регистрация</h1>
        <form action="{{ route('user.register') }}" method="POST" id="register-form">
            @csrf
            <div class="grid gap-6 mb-6 lg:grid-cols-2">
                <div>
                    <label for="firstname"
                           class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Имя</label>
                    <input type="text" id="firstname" name="firstname" value="{{ old('firstname') }}"
                           class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-indigo-500 focus:border-indigo-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-indigo-500 dark:focus:border-indigo-500{{ $errors->has('firstname') ? ' border-red-500' : '' }}">
                    @error('firstname')
                    <div class="error text-red-500 text-xs">{{ $message }}</div>
                    @enderror
                </div>
                <div>
                    <label for="lastname" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Фамилия</label>
                    <input type="text" id="lastname" name="lastname" value="{{ old('lastname') }}"
                           class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-indigo-500 focus:border-indigo-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-indigo-500 dark:focus:border-indigo-500{{ $errors->has('lastname') ? ' border-red-500' : '' }}">
                    @error('lastname')
                    <div class="error text-red-500 text-xs">{{ $message }}</div>
                    @enderror
                </div>
                <div>
                    <label for="phone" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Номер
                        телефона</label>
                    <input type="tel" id="phone" name="phone" value="{{ old('phone') }}"
                           class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-indigo-500 focus:border-indigo-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-indigo-500 dark:focus:border-indigo-500{{ $errors->has('phone') ? ' border-red-500' : '' }}">
                    @error('phone')
                    <div class="error text-red-500 text-xs">{{ $message }}</div>
                    @enderror
                </div>
                <div>
                    <label for="email"
                           class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Email</label>
                    <input type="email" id="email" name="email" value="{{ old('email') }}"
                           class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-indigo-500 focus:border-indigo-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-indigo-500 dark:focus:border-indigo-500{{ $errors->has('email') ? ' border-red-500' : '' }}">
                    @error('email')
                    <div class="error text-red-500 text-xs">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="mb-6">
                <label for="education" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Образование</label>
                <select id="education" name="education"
                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-indigo-500 focus:border-indigo-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-indigo-500 dark:focus:border-indigo-500">
                    @foreach($education_type_list as $key => $type)
                        <option value="{{ $key }}" {{old('education') == $key ? 'selected' : ''}}>{{ $type }}</option>
                    @endforeach
                </select>
                @error('education')
                <div class="error text-red-500 text-xs">{{ $message }}</div>
                @enderror
            </div>
            <div class="flex items-start mb-6">
                <div class="flex items-center h-5">
                    <input type="hidden" value="0" name="agree">
                    <input type="checkbox" id="agree" name="agree" {{ old('agree') ? 'checked' : '' }}
                           class="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-indigo-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-indigo-600 dark:ring-offset-gray-800">
                </div>
                <label for="agree" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-400">Я даю согласие на
                    обработку моих личных данных.</label>
                @error('agree')
                    <div class="error text-red-500 text-xs">{{ $message }}</div>
                @enderror
            </div>

            <div class="flex justify-center items-center">
                <button type="submit"
                        class="text-white bg-indigo-600 hover:bg-indigo-800 focus:ring-4 focus:outline-none focus:ring-indigo-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-indigo-600 dark:hover:bg-indigo-700 dark:focus:ring-indigo-800">
                    Отправить
                </button>
            </div>

            <div class="text-xs flex justify-center items-center mt-3 font-thin">
                *Баллы за заполнение полей будут рассчитаны на стороне приложения
            </div>
        </form>
    </div>
@endsection

@push('scripts')
    <script>
        const registerForm = document.getElementById("register-form")
        registerForm.onsubmit = function (e) {
            e.preventDefault();
            const self = this;
            const agreeEl = self.querySelector('input[type="checkbox"]');
            if (agreeEl.checked) {
                agreeEl.value = 1;
            }
            self.submit();
        };
    </script>
@endpush
