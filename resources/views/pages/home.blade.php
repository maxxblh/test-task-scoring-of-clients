@extends('layouts.app')

@section('content')
    <div class="py-6 md:py-12">
        <div class="container px-4 mx-auto">
            <div class="text-center max-w-2xl mx-auto">
                <h1 class="text-3xl md:text-4xl font-medium mb-2 text-gray-600">Тестовое задание "Приложение для расчета скоринга
                    клиентов".</h1>
                <div class="mt-4">
                    <img src="{{ asset('images/home.jpg') }}" alt="cat" class="mx-auto d-block max-w-full rounded shadow-md">
                </div>
            </div>
        </div>
    </div>
@endsection
