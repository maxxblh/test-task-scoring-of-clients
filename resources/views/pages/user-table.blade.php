@extends('layouts.app')

@section('content')
    <div class="relative overflow-x-auto mx-12 p-16 shadow-md sm:rounded-lg">
        <h1 class="font-bold text-xl text-indigo-600 flex justify-center items-center mb-6">Список пользователей</h1>

        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">
                    ID
                </th>
                <th scope="col" class="px-6 py-3">
                    Имя
                </th>
                <th scope="col" class="px-6 py-3">
                    Фамилия
                </th>
                <th scope="col" class="px-6 py-3">
                    Номер телефона
                </th>
                <th scope="col" class="px-6 py-3">
                    Email
                </th>
                <th scope="col" class="px-6 py-3">
                    Образование
                </th>
                <th scope="col" class="px-6 py-3">
                    Баллы
                </th>
                <th scope="col" class="px-6 py-3">
                    <span class="sr-only">Edit</span>
                </th>
                <th scope="col" class="px-6 py-3">
                    <span class="sr-only">Remove</span>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $key => $user)
                <tr class="odd:bg-white even:bg-gray-50 odd:dark:bg-gray-800 even:dark:bg-gray-700 border-b dark:bg-gray-800 dark:border-gray-700">
                    <td class="px-6 py-4">
                        {{ $user->id }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $user->firstname }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $user->lastname }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $user->phone }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $user->email }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $user->education_name }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $user->scoreResult ? $user->scoreResult->score_total : 0 }}
                    </td>
                    <td class="px-6 py-4 text-right">
                        <a href="{{ route('user.edit.index', $user->id) }}"
                           class="font-medium text-green-600 dark:text-green-500 hover:underline">
                            <svg width="16" height="16" fill="currentColor"
                                 class="bi bi-pencil" viewBox="0 0 16 16">
                                <path
                                    d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                            </svg>
                        </a>
                    </td>
                    <td class="px-6 py-4 text-right">
                        <button onclick="confirmDelete({{ $user->id }})"
                                class="font-medium text-red-700 dark:text-red-500 hover:underline">x
                        </button>
                    </td>
                </tr>
            @endforeach

            @if($users->isEmpty())
                <tr class="odd:bg-white even:bg-gray-50 odd:dark:bg-gray-800 even:dark:bg-gray-700 border-b dark:bg-gray-800 dark:border-gray-700">
                    <td colspan="7" class="px-6 py-4">
                        <div class="font-medium text-gray-800 text-center">Данные отсутствуют</div>
                    </td>
                </tr>
            @endif

            </tbody>
        </table>

        <div class="flex justify-center mt-2">
            {{ $users->links('pagination::tailwind') }}
        </div>
    </div>

    <div id="modal-window"
         class="hidden bg-slate-800 bg-opacity-50 flex justify-center items-center absolute top-0 right-0 bottom-0 left-0">
        <div class="bg-white px-16 py-14 rounded-md text-center">
            <h1 id="modal-message" class="text-xl mb-4 font-bold text-slate-500">Удалить пользователя?</h1>
            <button id="modal-confirm" class="bg-red-500 px-7 py-2 ml-2 rounded-md text-md text-white">Да</button>
            <button id="modal-cancel" class="bg-indigo-500 px-4 py-2 rounded-md text-md text-white">Нет</button>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function confirmDelete(userId) {
            const modal = document.getElementById("modal-window");
            modal.classList.remove("hidden");

            document.getElementById("modal-cancel").addEventListener('click', () => {
                modal.classList.add("hidden");
            });

            document.getElementById("modal-confirm").addEventListener('click', () => {
                window.location.href = `/users/remove/${userId}`;
                modal.classList.add("hidden");
            });
        }
    </script>
@endpush
