@if(session()->has('success'))
    <div id="message" class="px-6 py-4 border-0 rounded relative mb-4 text-green-700 bg-green-100">
    <span class="inline-block align-middle mr-8">
    {{ session()->get('success') }}
    </span>
        <button
            id="close-button"
            class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none">
            <span>×</span>
        </button>
    </div>
@endif

@if(session()->has('fail'))
    <div id="message" class="px-6 py-4 border-0 rounded relative mb-4 text-red-700 bg-red-100">
    <span class="inline-block align-middle mr-8">
    {{ session()->get('fail') }}
    </span>
        <button
            id="close-button"
            class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none">
            <span>×</span>
        </button>
    </div>
@endif

@push('scripts')
    <script>
        const flashMessage = document.getElementById("message");
        if (flashMessage) {
            document.getElementById("close-button")
                .addEventListener("click", () => {
                    flashMessage.hidden = true;
                }, false);
        }
    </script>
@endpush
