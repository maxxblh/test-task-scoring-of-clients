<?php

namespace Database\Seeders;

use App\Models\ScoreResult;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::withoutEvents(function () {
            return User::factory(10)
                ->has(ScoreResult::factory()->count(1))
                ->create();
        });
    }
}
