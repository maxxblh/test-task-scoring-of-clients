<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ScoreResultFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'score_phone'     => $this->faker->randomElement([1, 3, 5, 10]),
            'score_email'     => $this->faker->randomElement([3, 6, 8, 10]),
            'score_education' => $this->faker->randomElement([5, 10, 15]),
            'score_agree'     => $this->faker->randomElement([0, 4]),
        ];
    }
}
