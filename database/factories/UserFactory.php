<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $email_domains = [
            '@gmail.com',
            '@yahoo.com',
            '@apple.com',
            '@outlook.com',
            '@yandex.ru',
            '@mail.ru',
            '@rambler.ru'
        ];
        return [
            'firstname' => $this->faker->firstName(),
            'lastname'  => $this->faker->lastName(),
            'phone'     => $this->faker->unique()->numerify('89#########'),
            'email'     => $this->faker->unique()->word() . $this->faker->randomElement($email_domains),
            'education' => $this->faker->numberBetween(1, 3),
            'agree'     => $this->faker->numberBetween(0, 1),
        ];
    }
}
