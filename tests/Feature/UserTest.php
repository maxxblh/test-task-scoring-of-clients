<?php

namespace Tests\Feature;

use App\Jobs\CalculateScoreJob;
use App\Models\ScoreResult;
use App\Models\User;
use App\Services\KodyClientService;
use App\Services\ScoreResultService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Bus;
use Mockery\MockInterface;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    public function test_register_success_user()
    {
        Bus::fake();

        $data = $this->getUserData();

        $response = $this->post(route('user.register'), $data);
        $response->assertStatus(302);

        $user = User::latest()->first();
        $this->assertNotEmpty($user);

        $this->assertUserParams($data, $user);

        Bus::assertDispatched(CalculateScoreJob::class);

        $this->mockKodyClientService();
        app(ScoreResultService::class)->calculate($user);
        $score = $user->scoreResult;
        $this->assertNotEmpty($score);

        $this->assertEquals($user->id, $score->user_id);
        $this->assertEquals(16, $score->score_total);
    }

    public function test_register_fail_user()
    {
        Bus::fake();

        $data = [
            'firstname' => '',
            'lastname'  => '',
            'phone'     => '',
            'email'     => '',
            'education' => '',
            'agree'     => '',
        ];

        $this->post(route('user.register'), $data)
            ->assertSessionHasErrors([
                'firstname',
                'lastname',
                'phone',
                'email',
                'education',
                'agree',
                ])
            ->assertStatus(302);
        Bus::assertNotDispatched(CalculateScoreJob::class);

        $data = [
            'firstname' => 'Иван',
            'lastname'  => 'Иванов',
            'phone'     => '97600000000',
            'email'     => 'test',
            'education' => 100,
            'agree'     => '1',
        ];
        $this->post(route('user.register'), $data)
            ->assertSessionHasErrors([
                'phone',
                'email',
                'education',
            ])
            ->assertStatus(302);
        Bus::assertNotDispatched(CalculateScoreJob::class);
    }

    public function test_edit_success_user()
    {
        Bus::fake();

        $user = $this->createUser();
        $data = $this->getUserData();
        $response = $this->put(route('user.edit', [$user]), $data);
        $response->assertStatus(302);
        $user->refresh();

        $this->assertUserParams($data, $user);

        Bus::assertDispatched(CalculateScoreJob::class);

        $this->mockKodyClientService();
        app(ScoreResultService::class)->calculate($user);
        $score = $user->scoreResult;
        $this->assertNotEmpty($score);

        $this->assertEquals($user->id, $score->user_id);
        $this->assertEquals(16, $score->score_total);
    }

    public function test_edit_fail_user()
    {
        Bus::fake();
        $user = $this->createUser();

        $data = [
            'firstname' => '',
            'lastname'  => '',
            'phone'     => '',
            'email'     => '',
            'education' => '',
            'agree'     => '',
        ];
        $this->put(route('user.edit', [$user]), $data)
            ->assertSessionHasErrors([
                'firstname',
                'lastname',
                'phone',
                'email',
                'education',
                'agree',
            ])
            ->assertStatus(302);

        Bus::assertNotDispatched(CalculateScoreJob::class);
    }

    public function test_edit_remove_user()
    {
        $user = $this->createUser();
        $score = $user->scoreResult;
        $this->get(route('user.remove', [$user]))
            ->assertStatus(302);

        $this->assertEmpty($user->fresh());
        $this->assertEmpty($score->fresh());
    }

    private function mockKodyClientService()
    {
        $this->mock(KodyClientService::class, function (MockInterface $mock) {
            $mock->shouldReceive('getOperatorName')->andReturn('');
        });
    }

    private function assertUserParams(array $expected, User $actual)
    {
        $this->assertEquals($expected['firstname'], $actual->firstname);
        $this->assertEquals($expected['lastname'], $actual->lastname);
        $this->assertEquals($expected['phone'], $actual->phone);
        $this->assertEquals($expected['email'], $actual->email);
        $this->assertEquals($expected['education'], $actual->education);
        $this->assertEquals($expected['agree'], $actual->agree);
    }

    private function createUser()
    {
        return User::withoutEvents(function () {
            return User::factory(1)
                ->has(ScoreResult::factory()->count(1))
                ->create()->first();
        });
    }

    private function getUserData(): array
    {
        return [
            'firstname' => 'Иван',
            'lastname'  => 'Иванов',
            'phone'     => '89600000000',
            'email'     => 'test@mail.ru',
            'education' => User::EDUCATION_TYPE_SECONDARY,
            'agree'     => '1',
        ];
    }
}
